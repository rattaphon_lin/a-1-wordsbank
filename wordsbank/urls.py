from django.conf.urls import url

from . import views

app_name = 'wordsbank'
urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'words_bank$', views.WordsBankView.as_view(), name='WordsBank'),
    url(r'words_bank/search$', views.search, name='search'),
    url(r'words_bank/(?P<word_text>\w+)/$', views.detail, name='detail'),
    url(r'save_word$', views.save_word, name='save_word'),
    url(r'upload$', views.upload, name='upload'),
    url(r'upload_file$', views.upload_file, name='upload_file'),
    url(r'download_file$', views.download_file, name='download_file'),
    url(r'delete_all$', views.delete_all, name='delete_all'),

]
# url(r'^(?P<question_id>[a-z]+)/$', views.DetailView.as_view(), name='detail'),
# Regular Expression
# [0-9] (+) so on...
# ?P(parameter)<...>

