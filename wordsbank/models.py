from django.db import models

# Create your models here.


class Word(models.Model):
    word_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.word_text


class Detail(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    type = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    example = models.CharField(max_length=200)

    def __str__(self):
        return self.type + self.description + self.example
