from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect , HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic
from .models import Word, Detail
from django.utils import timezone
import csv, os
from django.core.files.storage import default_storage


class HomeView(generic.ListView):
    template_name = 'wordsbank/home.html'

    def get_queryset(self):

        return None


def search(request):
    word = request.POST['search']
    return HttpResponseRedirect(reverse('wordsbank:detail', args={word}))


def detail(request, word_text):

    try:
        Word.objects.get(word_text=word_text)
    except Word.DoesNotExist:
        return render(request, 'wordsbank/detail.html', {
            'error_message': "Word not found!.",
        })
    else:
        word_show = Word.objects.get(word_text=word_text)
        return render(request, 'wordsbank/detail.html', {'word': word_show})


def save_word(request):

    word_save = str(request.POST['word'])
    type_save = str(request.POST['type'])
    description_save = str(request.POST['description'])
    example_save = str(request.POST['example'])

    if word_save == "" or description_save == "" or example_save == "":
        return render(request, 'wordsbank/home.html', {
            'error_message': "Please fill in the form completely.",
        })
    else:
        if not Word.objects.filter(word_text=word_save):  # Don't have word
            new_word = Word(word_text=word_save, pub_date=timezone.now())
            new_word.save()
            new_word.detail_set.create(type=type_save, description=description_save, example=example_save)
        else:
            word_current = Word.objects.get(word_text=word_save)
            word_current.detail_set.create(type=type_save, description=description_save, example=example_save)

    return HttpResponseRedirect(reverse('wordsbank:WordsBank'))


def upload(request):
    return render(request,'wordsbank/upload.html')


def upload_file(request):
    dir_path = 'wordsbank/static/wordsbank/'
    csv_file = request.FILES['csvfile']
    file_path = default_storage.save(dir_path, csv_file)
    os.rename(file_path, "{}word_bank.csv".format(dir_path))
    with open("{}word_bank.csv".format(dir_path), encoding='utf-8') as csvfile:
        dataReader = csv.DictReader(csvfile)

        for row in dataReader:
            if row["Word"] != "":
                word = row["Word"]
                if Word.objects.filter(word_text=word):  # if have word
                    word_add = Word.objects.get(word_text=word)  # get word
                else:  # if not have word
                    word_add = Word(word_text=word, pub_date=timezone.now())  # create new word
                word_add.save()

            if row["Type"] != "":
                type = row["Type"]
                word_add.save()

            if row["Description"] != "":
                description = row["Description"]
                word_add.save()

            if row["Example"] != "":
                example = row["Example"]
                word_add.detail_set.create(type=type, description=description, example=example)
                word_add.save()

    return HttpResponseRedirect(reverse('wordsbank:WordsBank'))


def download_file(request):
    with open('wordsbank/static/wordsbank/word_bank.csv', 'w')as csvfile:
                fieldnames = ['Word', 'Type', 'Description', 'Example']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                word_all = Word.objects.order_by('word_text')

                for word in word_all:
                    current_word = Word.objects.get(word_text=word)
                    current_detail = current_word.detail_set.all()
                    if len(current_detail) > 1:
                        j = 1  # use for count if len(crrent_detail) > 1 for create table that in row 'Word' must empty
                        for info in current_detail:
                            if j > 1:  # new line that 'Word' must empty
                                writer.writerow(
                                    dict(Word='', Type=info.type, Description=info.description, Example=info.example))
                            else:
                                writer.writerow(
                                    dict(Word=word, Type=info.type, Description=info.description, Example=info.example))
                            j += 1

                    else:
                        for info in current_detail:
                            writer.writerow(
                                dict(Word=word, Type=info.type, Description=info.description, Example=info.example))

    return HttpResponseRedirect(reverse('wordsbank:upload'))


def delete_all(request):
    word_all = Word.objects.all()
    word_all.delete()
    return render(request,'wordsbank/words_bank.html')



class WordsBankView(generic.ListView):
    template_name = 'wordsbank/words_bank.html'
    context_object_name = 'latest_word_list'

    def get_queryset(self):

        return Word.objects.order_by('word_text')