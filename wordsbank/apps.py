from django.apps import AppConfig


class WordsbankConfig(AppConfig):
    name = 'wordsbank'
